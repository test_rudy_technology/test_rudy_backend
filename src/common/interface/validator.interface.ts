export interface IPaginationValidator {
    getLimit(): number

    getPage(): number
}
