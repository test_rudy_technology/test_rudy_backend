export interface IPaginationFilter {
    limit: number
    page: number
}

export interface IAttachmentSchema {
    data: {
        link: string
    }
    alt: string
    size: number
    name: string
    originalName: string
    contentType: string
    contentBytes?: string
}