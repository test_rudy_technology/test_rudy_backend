import { Injectable } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { ExtractJwt, Strategy, VerifiedCallback } from 'passport-jwt'
import * as _ from 'lodash'
import { IUserPayloadJWT } from '../domain/login/interface/service.interface'

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        private readonly _jwtSecret: string,
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: _jwtSecret,
        })
    }

    validate(payload: any, verify: VerifiedCallback) {
        const userId = _.get(payload, 'id')
        const username = _.get(payload, 'username')
        const sessionId = _.get(payload, 'sessionId')
        const jwtPayload: IUserPayloadJWT = {
            id: userId,
            username,
            sessionId,
        }
        return verify(null, jwtPayload, payload.iat)
    }
}