import { IsNumberString, IsOptional } from 'class-validator'
import { IPaginationValidator } from './interface/validator.interface'
import * as _ from 'lodash'
import { ApiProperty } from '@nestjs/swagger'

export class PaginatorValidator implements IPaginationValidator {

    @ApiProperty({
        name: 'limit',
        required: false,
        type: String,
        example: '10',
    })
    @IsOptional()
    @IsNumberString()
    private readonly limit: string

    @ApiProperty({
        name: 'page',
        required: false,
        type: String,
        example: '1',
    })
    @IsOptional()
    @IsNumberString()
    private readonly page: string

    public getLimit(): number {
        return _.isEmpty(this.limit) || _.isNil(this.limit) ? null : _.toNumber(this.limit)
    }

    public getPage(): number {
        return _.isEmpty(this.page) || _.isNil(this.page) ? null : _.toNumber(this.page)
    }
}

