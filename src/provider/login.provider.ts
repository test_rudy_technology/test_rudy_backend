import { Provider } from '@nestjs/common'
import { providerNames } from './provider.name'
import { LoginService } from '../domain/login/login.service'
import { JwtService } from '@nestjs/jwt'
import { IUserManagementRepository } from '../domain/user-management/interface/repository.interface'

export const loginServiceProvider: Provider = {
    provide: providerNames.LOGIN_SERVICE,
    inject: [
        providerNames.JWT_SERVICE,
        providerNames.USER_MANAGEMENT_REPOSITORY,
    ],
    useFactory: (
        jwtService: JwtService,
        usersRepository: IUserManagementRepository,
    ) => {
        return new LoginService(
            jwtService,
            usersRepository,
        )
    },
}