import { Global, Module, Provider } from '@nestjs/common'
import { configProvider, jwtServiceProvider, mongoConnection } from '../provider/common.provider'

const globalProviders: Provider[] = [
    configProvider,
    mongoConnection,
    ...jwtServiceProvider,
]

@Global()
@Module({
    controllers: [],
    providers: globalProviders,
    exports: globalProviders,
})

export class CommonModule {}
