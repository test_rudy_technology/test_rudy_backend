import { Body, Controller, Inject, Logger, Post, Req } from '@nestjs/common'
import { ApiOperation, ApiTags } from '@nestjs/swagger'
import { providerNames } from '../../provider/provider.name'
import { IConfig } from '../../common/interface/config.interface'
import { ILoginService } from '../../domain/login/interface/service.interface'
import { LoginValidator } from './login.validator'
import { map } from 'rxjs'
import { Request } from 'express'

@ApiTags('Login')
@Controller('/login')
export class LoginController {
    private readonly _logger: Logger

    constructor(
        @Inject(providerNames.CONFIG)
        private readonly _config: IConfig,
        @Inject(providerNames.LOGIN_SERVICE)
        private readonly _loginService: ILoginService,
    ) {
        this._logger = new Logger(`LoginController`)
    }

    @ApiOperation({
        summary: 'login',
    })
    @Post('/')
    public login(
        @Req() request: Request,
        @Body() body: LoginValidator,
    ) {
        const requestHeader = request.headers
        return this._loginService.login(body, requestHeader).pipe(
            map((token: string) => {
                return {token}
            }),
        )
    }
}