import { ApiBearerAuth, ApiConsumes, ApiOperation, ApiTags } from '@nestjs/swagger'
import {
    BadRequestException,
    Body,
    Controller,
    Get,
    Inject,
    Param,
    Post,
    UploadedFiles,
    UseGuards, UseInterceptors,
} from '@nestjs/common'
import { providerNames } from '../../provider/provider.name'
import { IUserManagementService } from '../../domain/user-management/interface/service.interface'
import { RegisterValidator } from './user-management.validator'
import { JwtAuthGuard } from '../../guards/jwt.guard'
import { FileFieldsInterceptor } from '@nestjs/platform-express'
import * as _ from 'lodash'

@ApiTags('User-Management')
@Controller('/user-management')
export class UserManagementController {
    constructor(
        @Inject(providerNames.USER_MANAGEMENT_SERVICE)
        private readonly _userManagementService: IUserManagementService,
    ) {
    }

    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('jwt-token')
    @ApiOperation({
        summary: 'get by id',
    })
    @Get('/:id')
    public getById(
        @Param('id') id: string,
    ) {
        return this._userManagementService.getById(id)
    }

    @ApiOperation({
        summary: 'register',
    })
    @Post('/')
    @UseInterceptors(
        FileFieldsInterceptor([
            {name: 'file', maxCount: 1},
        ], {
            fileFilter(req: any, file: { fieldname: string; originalname: string; encoding: string; mimetype: string; size: number; destination: string; filename: string; path: string; buffer: Buffer }, callback: (error: (Error | null), acceptFile: boolean) => void) {
                const type = ['image/jpg', 'image/jpeg', 'image/png']
                if (!type.some(value => {
                    return _.includes(file.mimetype, value)
                })) {
                    return callback(new BadRequestException('Only jpeg or png files are allowed!'), false)
                }
                callback(null, true)
            },
        }))
    @ApiConsumes('multipart/form-data')
    public register(
        @Body() body: RegisterValidator,
        @UploadedFiles() files: Express.Multer.File[],
    ) {
        return this._userManagementService.register(body, files)
    }
}