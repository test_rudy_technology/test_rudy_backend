import { ObjectId } from 'bson'
import { IAttachmentSchema } from '../../common/interface/schema.interface'

export interface IUserManagementSchema {
    _id: ObjectId
    username: string
    password: string
    name: string
    gender: GenderEnum
    address: string
    email: string
    telephone: string
    profile: IAttachmentSchema
    createdAt: Date
}

export enum GenderEnum {
    MALE = 'male',
    FEMALE = 'female',
    OTHER = 'other',
}