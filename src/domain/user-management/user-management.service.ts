import { IUserManagementService } from './interface/service.interface'
import { Observable } from 'rxjs'
import { IUserManagementRepository } from './interface/repository.interface'
import { UserManagementBuilder } from './user-management.builder'
import { IRegisterValidator } from './interface/validator.interface'
import { IUserManagementModel } from './interface/model.interface'
import * as _ from 'lodash'
import * as fs from 'fs'
import * as Path from 'path'
import * as moment from 'moment'
import * as uuid from 'uuid'
import { InternalServerErrorException } from '@nestjs/common'
import { IAttachmentSchema } from '../../common/interface/schema.interface'

export class UserManagementService implements IUserManagementService {
    constructor(
        private readonly _usersRepository: IUserManagementRepository,
    ) {
    }

    public register(data: IRegisterValidator, files: Express.Multer.File[]): Observable<string> {
        const file = _.get(files, 'file[0]', null)
        const fileSchema = this._saveFile(file)
        const newUser = new UserManagementBuilder()
        newUser.setUsername(data.getUsername())
        newUser.setPassword(data.getPassword())
        newUser.setName(data.getName())
        newUser.setGender(data.getGender())
        newUser.setAddress(data.getAddress())
        newUser.setEmail(data.getEmail())
        newUser.setTelephone(data.getTelephone())
        newUser.setProfile(fileSchema)
        return this._usersRepository.insert(newUser.build())
    }

    public getById(id: string): Observable<IUserManagementModel> {
        return this._usersRepository.getById(id)
    }

    private _saveFile(file: Express.Multer.File, compareFile?): IAttachmentSchema {
        if (_.isNil(file)) {
            return null
        }
        if (!_.isNil(compareFile)) {
            if (_.isEqual(file.originalname, compareFile.name) && _.isEqual(file.size, compareFile.size) && _.isEqual(file.mimetype, compareFile.contentType)) {
                return compareFile
            }
        }
        const folderName = moment().format('YYYYMMDD')
        let savePath = `./upload/${folderName}`
        if (!fs.existsSync(savePath)) {
            fs.mkdirSync(savePath, {recursive: true})
        }
        let fileName = `${uuid.v4()}${Path.extname(file.originalname)}`
        savePath += `/${fileName}`
        try {
            fs.writeFileSync(savePath, file.buffer)
        } catch (err) {
            if (err) {
                throw new InternalServerErrorException(`Cannot write file ${file.originalname}`)
            }
        }
        return {
            data: {
                link: savePath,
            },
            alt: fileName,
            name: fileName,
            originalName: file.originalname,
            size: file.size,
            contentType: file.mimetype,
        }
    }
}