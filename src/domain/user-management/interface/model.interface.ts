import { IEntity } from '../../../common/interface/entity.interface'
import { GenderEnum } from '../../../repository/user-management/user-management.schema'
import { IAttachmentSchema } from '../../../common/interface/schema.interface'

export interface IUserManagementModel extends IEntity {
    getUsername(): string
    setUsername(username: string): void

    getPassword(): string
    setPassword(password: string): void

    getName(): string
    setName(name: string): void

    getGender(): GenderEnum
    setGender(gender: GenderEnum): void

    getAddress(): string
    setAddress(address: string): void

    getEmail(): string
    setEmail(email: string): void

    getTelephone(): string
    setTelephone(telephone: string): void

    getProfile(): IAttachmentSchema
    setProfile(file: IAttachmentSchema)

}