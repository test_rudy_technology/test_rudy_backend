import { IUserManagementModel } from './model.interface'
import { Observable } from 'rxjs'
import { IRepository } from '../../../common/interface/repository.interface'

export interface IUserManagementRepository extends IRepository<IUserManagementModel> {
    getById(id: string): Observable<IUserManagementModel>
}