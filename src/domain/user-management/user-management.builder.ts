import { IUserManagementModel } from './interface/model.interface'
import { UserManagementModel } from './user-management.model'
import { GenderEnum } from '../../repository/user-management/user-management.schema'
import { IAttachmentSchema } from '../../common/interface/schema.interface'

export class UserManagementBuilder {
    private readonly _model: IUserManagementModel

    constructor() {
        this._model = new UserManagementModel()
    }

    public build(): IUserManagementModel {
        return this._model
    }

    public setUsername(username: string) {
        this._model.setUsername(username)
        return this
    }

    public setPassword(password: string) {
        this._model.setPassword(password)
        return this
    }

    public setName(name: string) {
        this._model.setName(name)
        return this
    }

    public setGender(gender: GenderEnum) {
        this._model.setGender(gender)
        return this
    }

    public setAddress(address: string) {
        this._model.setAddress(address)
        return this
    }

    public setEmail(email: string) {
        this._model.setEmail(email)
        return this
    }

    public setTelephone(telephone: string) {
        this._model.setTelephone(telephone)
        return this
    }

    public setProfile(file: IAttachmentSchema) {
        this._model.setProfile(file)
        return this
    }
}