# test_rudy_backend


## 1. ให้อธิบายว่าจะใช้วิธีการอะไรได้บ้างในการป้องกัน brute force attack หรือเดารหัสผ่านใน login form
1
```
ตั้งรหัสผ่านที่มีความซับซ้อน โดยรหัสผ่านไม่ควรเป็นคำศัพท์ วันเดือนปีเกิด เลขที่อยู่ และควรมีความยาวอย่างน้อย 8 ตัวอักษร มีการใช้ตัวพิมพ์ใหญ่ เล็ก มีอักขระพิเศษ และตัวเลข ผสมอยู่ในรหัสผ่าน และเปลี่ยนรหัสผ่านบ่อย ๆ เช่น ทุกๆ 30 หรือ 90 วัน
```
2
```
limit จำนวนการ login หาก login ผิดติดต่อกันเกินกี่ครั้ง จะถูกห้าม login ตามเวลาที่กำหนดไว้ หรือติดต่อ admin เพื่อปลดล็อค
```
***
## 2. จงเขียนตัวอย่าง sql query ในโค้ด php โดยให้มีชุดคำสั่งที่ช่วยป้องกัน sql injection (ตั้งชื่อตารางชื่อฟิลด์ด้วยตัวเองได้เลย)
ใน nodejs ใช้ placeholders
```
connection.query('UPDATE users SET foo = ?, bar = ?, baz = ? WHERE id = ?', ['a', 'b', 'c', userId], function (error, results, fields) {
if (error) throw error;
// ...
});
```

***
## 3. จงเขียน sql query ที่มี sub query ในตำแหน่งต่างๆ อย่างน้อยสองแบบ (ตั้งชื่อตารางชื่อฟิลด์ด้วยตัวเองได้เลย)
1
```
UPDATE tb_product SET status = 0 WHERE id = (SELECT id FROM tb_shop WHERE name = 'rudy shop' LIMIT 1)
```
2
```
SELECT *
FROM (SELECT * FROM b) AS a
WHERE a.price = 1000
```
***
## 4. จากตาราง tb_product(id,name,status,shop_id) และ tb_shop(id,name) จงเขียน โค้ด select เพื่อแสดงสินค้าของร้าน ที่มีชื่อร้าน "rudy shop"
```
SELECT * FROM tb_product 
LEFT JOIN tb_shop ON (tb_product.shop_id = tb_shop.id)
WHERE tb_shop.name = 'rudy shop'
```
***
## 5. เขียนคำสั่ง update สินค้าทุกตัวของร้าน "rudy shop" ให้มี status='0'
```
UPDATE tb_product SET status = 0 WHERE id = (SELECT id FROM tb_shop WHERE name = 'rudy shop' LIMIT 1)
```
***
## 6. จงเขียน function ของ code sql เพื่อปรับรูปแบบการ select ข้อมูล ตามประเภทข้อมูลดังนี้เพื่อให้ได้ผลลัพธืดังตัวอย่าง 
- type date ให้แสดงผลเป็น dd/mm/YYYY 
```
SELECT DATE_FORMAT(created_at, '%d-%m-%Y') FROM tb_product
```
- type float,double ให้แสดงผลเป็น x,xxx,xxx.xx
```
SELECT FORMAT(price,2) FROM tb_product
```

***
## 7. จงเขียน code function php ในการคำนวณผลลัพธ์ใบเสนอราคาโดยหัวข้อมีดังนี้
- ราคาสินค้ารวม = สามารถตั้งเองได้
- ส่วนลดรวม = สามารถตั้งเองได้
- ราคาสินค้าหลังส่วนลด
- ภาษีมูลค่าเพิ่ม 7 %
- ราคารวมสุทธิ
```
function calculateVat(totalPrice, discount) {
    let discountPrice = totalPrice - discount
    let vat = discountPrice * 0.07
    let net = discountPrice + vat
    return {
        discountPrice: discountPrice,
        vat: vat,
        net: net
    }
}
```